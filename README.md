# FaceRecognition
Student project about trying face recognition methods and implementing them using CNN

if the Eigen lib is needed, compile with
g++ file.cpp -I eigen-3.4.0/ -o a.out;

to use the filesystem library, compile with
g++ file.cpp -o a.out -std=c++17 -lstdc++fs;
